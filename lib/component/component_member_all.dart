import 'package:flutter/material.dart';
import 'package:wone_app/model/member_information_item.dart';

class ComponentMemberAll extends StatelessWidget {
  const ComponentMemberAll({
    super.key,
    required this.memberInformationItem,
    required this.callback

  });
  final MemberInformationItem memberInformationItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Text(
            '${memberInformationItem.id}',
          ),
          Text(
            memberInformationItem.username,
          ),
          Text(
            '${memberInformationItem.birthDate}'
          ),
          Text(
              memberInformationItem.password
          ),
          Text(
              memberInformationItem.cardGroup
          ),
          Text(
              memberInformationItem.cardNumber
          ),
          Text(
              memberInformationItem.endDate
          ),
        ],
      ),
    );
  }
}
