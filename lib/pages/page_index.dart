import 'package:flutter/material.dart';
import 'package:wone_app/pages/answer_create_page.dart';
import 'package:wone_app/pages/calendar_page.dart';
import 'package:wone_app/pages/guide_page.dart';
import 'package:wone_app/pages/main_page.dart';
import 'package:wone_app/pages/member_info_page.dart';
import 'package:wone_app/pages/notice_page.dart';
import 'package:wone_app/pages/setting_page.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0;

  // 페이지
  static const List<Widget> _widgetOptions = <Widget>[
    MainPage(),
    CalenderPage(),
    SettingPage(),
    MemberInfoPage(id: 2), /** 단수 R 불러올 멤버의 id 입력해 줘야함 **/
    AnswerCreatePage(),
    NoticePage(),
    GuidePage(),
  ];

  // 아이콘
  List<BottomNavigationBarItem> _icons = [
    BottomNavigationBarItem(
      icon: Icon(
          Icons.home,
      ),
      label: '홈',
    ),
    BottomNavigationBarItem(
      icon: Icon(
          Icons.calendar_today,
      ),
      label: '캘린더',
    ),
    BottomNavigationBarItem(
      icon: Icon(
          Icons.person,
      ),
      label: '개인 정보',
    ),
    BottomNavigationBarItem(
      icon: Icon(
          Icons.settings,
      ),
      label: '설정',
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed, // 아이콘 추가시 하얘지는 현상 제거
        showSelectedLabels: false, // 라벨 제거
        showUnselectedLabels: false, // 라벨 제거
        items: _icons,
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xff3498db),
        onTap: _onItemTapped,
      ),
    );
  }
}
