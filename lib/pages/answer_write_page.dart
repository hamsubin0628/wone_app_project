import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';

class AnswerWritePage extends StatefulWidget {
  const AnswerWritePage({super.key});

  @override
  State<AnswerWritePage> createState() => _AnswerWritePageState();
}

class _AnswerWritePageState extends State<AnswerWritePage> {
  @override
  Widget build(BuildContext context) {
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,
        shadowColor: Colors.black.withOpacity(appbarShadow),
        elevation: 2,
        title: Text(
          '문의 작성하기',
          style: TextStyle(
              fontFamily: 'NotoSans_Bold',
              fontSize: appbarFontSize,
              letterSpacing: fontLetterSpacing,
              color: colorPrimary
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),

      body: ListView(
        children: [
          Container(
            /** 문의사항 입력 폼 **/
            padding: EdgeInsets.fromLTRB(20, 5, 20, 0),
            child: Column(
              children: [
                  SizedBox(
                  height: mediaQueryHeight / 1.5,
                  child: TextField(
                    style: TextStyle(
                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                      fontSize: fontSizeMid,
                      color: colorLightBlack,
                      letterSpacing: fontLetterSpacing,
                    ),
                    expands: true, /** 높이 먹이려면 확장해줘야함 **/
                    textAlignVertical: TextAlignVertical(y: -1),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                    decoration: InputDecoration(
                        hintText: '문의사항을 입력해 주세요.',
                        hintStyle: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMid,
                            color: colorGrey,
                            letterSpacing: fontLetterSpacing,
                        ),
                        enabledBorder: InputBorder.none, /** 보더 숨기기 **/
                      focusedBorder: InputBorder.none, /** 활성화 했을때 숨기기 **/
                    ),
                  )
                )
              ],
            ),
          ),
          /** 구분선 **/
          Container(
            margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
            height: 1,
            color: colorLightGrey,
          ),
          Container(
            /** 작성하기 버튼 **/
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(100, 45),
                    backgroundColor: colorPrimary,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)
                    ),
                  ),
                  child: Text(
                    '작성하기',
                    style: TextStyle(
                      fontSize: fontSizeSm,
                      color: Colors.white,
                      letterSpacing: fontLetterSpacing,
                      fontFamily: 'NotoSans_NotoSansKR-SemiBold'
                    ),
                  ),
                ),
              ],
            )
          )
        ],
      ),
    );
  }
}
