import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';


class NoticePage extends StatefulWidget {
  const NoticePage({super.key});

  @override
  State<NoticePage> createState() => _NoticePageState();
}

class _NoticePageState extends State<NoticePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "공지사항",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: ListView(
        children: [
          Container(
              child: ExpansionTile(
                  title: Text('Wone 서비스 이용 약관 변경 안내',
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      color: colorLightBlack,
                      fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                      letterSpacing: fontLetterSpacing
                    ),
                  ),
                initiallyExpanded: true, // 들어오자마자 펼쳐져 있을거냐
                backgroundColor: Colors.white,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(18, 0, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 80,
                          child: Text(
                            '안녕하세요. Wone팁입니다.\n'
                                '2023년 12월 10일부로 Wone 서비스 이용약관이\n'
                                '변경됨을 공지드립니다.',
                            style: TextStyle(
                                fontSize: fontSizeSm,
                                color: colorGrey,
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                letterSpacing: fontLetterSpacing
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
          ),
          Container(
            child: ExpansionTile(
              title: Text('WONE 1.3 정기 업데이트 소식',
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: colorLightBlack,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing
                ),
              ),
              initiallyExpanded: false,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(18, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 80,
                        child: Text(
                          '안녕하세요. Wone팁입니다.\n'
                              '2023년 12월 10일부로 Wone 서비스 이용약관이\n'
                              '변경됨을 공지드립니다.',
                          style: TextStyle(
                              fontSize: fontSizeSm,
                              color: colorGrey,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              letterSpacing: fontLetterSpacing
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            child: ExpansionTile(
              title: Text('WONE 1.2 정기 업데이트 소식',
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: colorLightBlack,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing
                ),
              ),
              initiallyExpanded: false,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(18, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 80,
                        child: Text(
                          '안녕하세요. Wone팁입니다.\n'
                              '2023년 12월 10일부로 Wone 서비스 이용약관이\n'
                              '변경됨을 공지드립니다.',
                          style: TextStyle(
                              fontSize: fontSizeSm,
                              color: colorGrey,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              letterSpacing: fontLetterSpacing
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            child: ExpansionTile(
              title: Text('WONE 1.1 정기 업데이트 소식',
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: colorLightBlack,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing
                ),
              ),
              initiallyExpanded: false,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(18, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 80,
                        child: Text(
                          '안녕하세요. Wone팁입니다.\n'
                              '2023년 12월 10일부로 Wone 서비스 이용약관이\n'
                              '변경됨을 공지드립니다.',
                          style: TextStyle(
                              fontSize: fontSizeSm,
                              color: colorGrey,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              letterSpacing: fontLetterSpacing
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            child: ExpansionTile(
              title: Text('WONE 1.0 정기 업데이트 소식',
                style: TextStyle(
                    fontSize: fontSizeBig,
                    color: colorLightBlack,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing
                ),
              ),
              initiallyExpanded: false,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(18, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 80,
                        child: Text(
                          '안녕하세요. Wone팁입니다.\n'
                              '2023년 12월 10일부로 Wone 서비스 이용약관이\n'
                              '변경됨을 공지드립니다.',
                          style: TextStyle(
                              fontSize: fontSizeSm,
                              color: colorGrey,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              letterSpacing: fontLetterSpacing
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
