import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/member_info_list_result.dart';
import 'package:wone_app/model/member_information_item.dart';

class RepoMemberAll {
  Future<MemberInfoListResult> getMemberAll() async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/member/detail/{id}';

    final response = await dio.get(_baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }));
    return MemberInfoListResult.fromJson(response.data);
  }
}