// 고객센터 단수용 작은 틀

class AnswerResponse {
  num id;
  String memberName;
  String askCreateDate;
  String askContent;
  String publicType;
  String? comment;
  num askPassword;

  AnswerResponse(this.id, this.memberName, this.askCreateDate, this.askContent, this.publicType, this.comment, this.askPassword);

  factory AnswerResponse.fromJson(Map<String, dynamic> json) {
    return AnswerResponse(
        json['id'],
        json['memberName'],
        json['askCreateDate'],
        json['askContent'],
        json['publicType'],
        json['comment'],
        json['askPassword']
    );
  }
}