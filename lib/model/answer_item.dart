// 고객센터 복수용 작은 틀

class AnswerItem   {
  num id;
  String memberName;
  String askContent;
  String publicType;


  AnswerItem(this.id, this.memberName, this.askContent, this.publicType);

  factory AnswerItem.fromJson(Map<String, dynamic> json) {
    return AnswerItem(
        json['id'],
        json['memberName'],
        json['askContent'],
        json['publicType']
    );
  }
}