import 'package:wone_app/model/member_information_item.dart';

class MemberInfoListResult {
  String msg;
  num code;
  List<MemberInformationItem> list;
  num totalCount;

  MemberInfoListResult(this.msg, this.code, this.list, this.totalCount);

  factory MemberInfoListResult.fromJson(Map<String, dynamic> json) {
    return MemberInfoListResult(
        json['msg'],
        json['code'],
        json['list'] !=null ? (json['list'] as List).map((e) => MemberInformationItem.fromJson(e)).toList() : [], // null이 아니면 (), null이면 [빈 배열]
        json['totalCount']
    );
    // json으로 [{name:'머시기'}, {name:'머시기'}]이런 string을 받아서 Object의 List로 바꾸고 난 후에
    // 그럼 어쨋든 List니까 한 뭉텡이씩 던져줄 수 있다.
    // 근데.. 한 뭉텡이 씩 던지겠다 하면서 한 뭉텡이 부르는거에 이름이 바로 e
    // 다 작은 그릇으로 바꿔치기 한 다음에
    // 다시 그것들을 삭 가져와서 리스트로 촥 하고 정리해줌
  }
}