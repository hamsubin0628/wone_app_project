import 'package:flutter/material.dart';

const double buttonRadius = 5;
const double boxRadius = 8;

// 로그인 박스 margin
const EdgeInsets edgeInsetsLogIn = EdgeInsets.fromLTRB(0, 3, 0, 3);

// 앱바 그림자
const double appbarShadow = 0.5;

// 회원가입 페이지 입력폼
const EdgeInsets edgeInsetsJoinBox = EdgeInsets.fromLTRB(20, 7, 20, 7);
// Text 부분
const EdgeInsets edgeInsetsJoinText = EdgeInsets.fromLTRB(20, 13, 20, 0);
// TextField 부분
const EdgeInsets edgeInsetsJoinTextField = EdgeInsets.fromLTRB(20, 0, 20, 0);

// 개인정보 페이지 정보
const EdgeInsets edgeInsetsMember = EdgeInsets.only(left: 23);

// 설정 페이지 목록
const EdgeInsets edgeInsetsSetting = EdgeInsets.all(5);
const EdgeInsets edgeInsetsSettingList = EdgeInsets.fromLTRB(10, 5, 10, 5);
