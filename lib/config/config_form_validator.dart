const String formErrorRequired = '이 값은 필수입니다.';
const String formErrorNumeric = '숫자만 입력해주세요.';
const String formErrorId = '올바른 아이디가 아닙니다.';
const String formErrorEqualPassword = '비밀번호가 일치하지 않습니다.';

String formErrorMinLength(int num) => '$num 자 이상 입력해주세요.';
String formErrorMaxLength(int num) => '$num 자 이하로 입력해주세요.';